# Validatie verkeerstellingen Flevoland

## Installatie

Op server:
```
mkdir -p ~/git
cd ~/git
git clone git@gitlab.com:keypointconsultancy/validatie-verkeerstellingen-flevoland.git
cd validatie-verkeerstellingen-flevoland
pip install -r requirements.txt
```

Maak een configuratiebestand:
```
cp configuratie_voorbeeld.json configuratie.json
```
Pas het `configuratie.json`-bestand naar wens aan. Met name de bestandslocatie voor de data (`data_path`).

## Gebruik

Start Jupyter op de server:
```
jupyter notebook --ip 0.0.0.0 --port 5000
```

Ga in de browser naar `http://localhost:5000` (bij lokaal gebruik) en voer het wachtwoord (of token) in.


## Git

Output van notebooks opschonen:
```
python3 -m nbconvert --clear-output *.ipynb **/*.ipynb
```

Code netjes maken:
```
black .
```
